package comboboxpackage;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class ComboBox extends JPanel {

    private JComboBox<String[]> petList;

    public ComboBox(int width, int height) {
        /// the call to super is just saying "Hey JPanel, set your self up so I can use you"
        super();

        /// setting the layour to null so I can configure it how I like
        this.setLayout(null);

        /// So the view is not transparent
        this.setOpaque(true);

        this.setBackground(Color.yellow);

        /// sets a border around the view
        this.setBorder(BorderFactory.createEmptyBorder(20, 20, 20, 20));

        /// just a function to setup the JComboBox
        this.initCombobox(width, height);
    }

    private void initCombobox(int width, int height) {

        /// the options of the JComboBox
        final String[] petStrings = { "Bird", "Cat", "Dog", "Rabbit", "Pig" };

        /// creating the JComboBox
        petList = new JComboBox(petStrings);

        /// this just sets the starting selection of my JComboBox 
        petList.setSelectedIndex(4);

        /** Whenever an action happens like selecting an option from the JComboBox, 
         *  I trigger the printPetName method 
         */
        petList.addActionListener(this::printPetName);

        /// sets border of the JComboBox
        petList.setBorder(BorderFactory.createLineBorder(Color.green, 1));

        /// sets bounds of the JComboBox
        petList.setBounds(0,0, width, height);

        /// adds the JComboBox to the panel
        add(petList);
    }

    /**
     * Gets called whenever a new selection has been made on the JComboBox and prints the name of the selection
     * @param e
     */
    private void printPetName(final ActionEvent e) {
        final String petName = petList.getSelectedItem().toString();
        System.out.println("Action Performed: " + petName);
    }
}

