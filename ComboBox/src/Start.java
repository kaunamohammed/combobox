public class Start {
    
    public static void main(final String[] args) {
        /** everything here is pretty standard and is just the entry point of your program
         * Here we've said the entry point is an AppFrame we've instantiated
         */
        javax.swing.SwingUtilities.invokeLater(() -> new AppFrame() );
    }

}