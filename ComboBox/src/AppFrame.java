import javax.swing.*;
import java.awt.*;

/// importing my package
import comboboxpackage.ComboBox;

public class AppFrame extends JFrame {

    /// creating a new panel
    Container combo_box = new ComboBox(200, 200);

    public AppFrame() {
        this.setDefaultBehaviour();
        /// this sets the frame to be visible 
        this.setVisible(true);
    }

    private void setDefaultBehaviour() { 

        // "this" just means the current object/class, its perfectly fine to call methods on the object without having to write that unless when its not totally clear

        /** 
         * Think of the ContentPane as a canvas for you to draw on.
         * When you call `setContentPane` you're overiding the ContentPane of the JFrame to another ContentPane you set
         * 
         */ 
        this.setContentPane(combo_box);

        /// Sets the minimum size of the frame
        this.setMinimumSize(new Dimension(200, 200));
        /// Sets the maximum size of the frame
        this.setMaximumSize(new Dimension(500, 500));

        this.setSize(new Dimension(300, 300));

        /// This gives the full dimension of your screen i.e. computer screen
        Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();

        /// Sets the location of your frame on the screen
        this.setLocation(
         (screenSize.width) / 4,
         (screenSize.height) / 4
        );
        
        /// the default way the program closes
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

}